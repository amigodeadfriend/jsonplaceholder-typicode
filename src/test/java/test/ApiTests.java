package test;

import io.restassured.response.Response;
import model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import services.UserService;
import utils.MyTestWatcher;

import java.util.List;

import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Api tests")
@Execution(ExecutionMode.CONCURRENT)
@ExtendWith(MyTestWatcher.class)
class ApiTests {
    private final UserService userService = new UserService();

    @Test
    @DisplayName("Test of response status")
    void testResponseStatus() {

        Response response = UserService.getResponseStatus();
        response.then()
                .assertThat().statusCode(SC_OK);
    }

    @Test
    @DisplayName("Test of response header")
    void testResponseHeader() {

        Response response = UserService.getResponseHeader();
        response.then()
                .assertThat().statusCode(SC_OK)
                .header("Content-type", notNullValue())
                .header("Content-type", equalTo("application/json; charset=utf-8"));
    }

    @Test
    @DisplayName("Test of response body size")
    void testResponseBodySize() {

        Response response = UserService.getResponseBodySize();
        response.then()
                .assertThat().statusCode(SC_OK)
                .body("size()", notNullValue())
                .body("size()", equalTo(10));
    }

    @Test
    @DisplayName("Test of user list size")
    void testUserListSize() {

        int expectedListSize = 10;
        int actualListSize = userService.getUserList().size();
        assertEquals(expectedListSize, actualListSize, "check of list size failed");
    }

    @Test
    @DisplayName("Test of user list IDs")
    void testUserListIDs() {

        List<User> userList = userService.getUserList();

        userList.forEach(user -> {
            assertNotNull(user.getId(), "User ID should not be null");
            assertFalse(user.getId().isEmpty(), "User ID should not be empty");
        });
    }
}
