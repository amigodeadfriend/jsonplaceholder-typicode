# jsonplaceholder-typicode


## Task 1

Create a test to verify a http status code:

- send the http request by using the GET method
- the URL is https://jsonplaceholder.typicode.com/users
- validation: status code of the obtained responce is 200 OK

## Task 2

Create a test to verify a http response header:

- send the http request by using the GET method
- the URL is https://jsonplaceholder.typicode.com/users
- validation: 
  - the content-type header exists in the obtained response
  - the value of the content-type header is application/json; charset = utf-8


## Task 3

Create a test to verify a http response header:

- send the http request by using the GET method
- the URL is https://jsonplaceholder.typicode.com/users
- validation: the content of the response body is the array of 10 users

***