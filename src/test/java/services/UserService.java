package services;


import io.restassured.response.Response;
import model.User;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;

public class UserService {

    public static Response getResponseStatus() {
        return given()
                .when()
                .get(UserEndpoint.USERS_ENDPOINT);
    }

    public static Response getResponseHeader() {
        return given()
                .when()
                .get(UserEndpoint.USERS_ENDPOINT);
    }

    public static Response getResponseBodySize() {
        return given()
                .when()
                .get(UserEndpoint.USERS_ENDPOINT);
    }

    public List<User> getUserList() {
        User[] users = given()
                .when()
                .get(UserEndpoint.USERS_ENDPOINT)
                .prettyPeek()
                .then()
                .assertThat().statusCode(SC_OK)
                .extract().as(User[].class);

        return Arrays.asList(users);
    }
}
